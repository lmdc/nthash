
[ "$1" = "" ] && echo "Usage: ./nthash.sh <password>"

printf $1 | iconv -f ASCII -t UTF-16LE | openssl dgst -md4
